def vowel_swapper(string):
    # ==============
    # Your code here
    new_string = ''
    vowel_dict = {'a':'4', 'e':'3', 'i':'!', 'o':'ooo', 'O': '000', 'u':'|_|'}
    for letter in string:
        letter2 = letter
        if letter2 != 'O':
            letter2 = letter2.lower()
        try:
            new_string = new_string + vowel_dict[letter2]
        except KeyError:
            new_string = new_string + letter
    return new_string      
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console