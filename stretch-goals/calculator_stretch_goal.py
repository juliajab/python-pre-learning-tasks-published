def calculator(a, b, operator):
    # ==============
    # Your code here

    if operator == "+":
        result = a + b
    elif operator == "-":
        result = a - b
    elif operator == "*":
        result = a * b
    elif operator == "/":
        result = a / b

    result = int(result)
    binary_result = ''
    remainder = None

    while int(result/2) > 0:
        remainder = result%2
        binary_result = str(remainder) + binary_result
        result = int(result/2)
    remainder = result%2
    binary_result = str(remainder) + binary_result

    return binary_result 
    # ==============
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
