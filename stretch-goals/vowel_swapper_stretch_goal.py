def vowel_swapper(string):
    # ==============
    # Your code here
    new_string = ''
    vowel_dict = {'a':'4', 'e':'3', 'i':'!', 'o':'ooo', 'O': '000', 'u':'|_|'}
    count_dict = {}
    for letter in string:
        letter2 = letter
        if letter2 != 'O':
            letter2 = letter2.lower()
        if letter2.lower() not in count_dict:
            count_dict[letter2.lower()] = 0
        else: 
            count_dict[letter2.lower()] = count_dict[letter2.lower()] + 1
        if count_dict.get(letter2.lower()) == 1:
            try:
                new_string = new_string + vowel_dict[letter2]
            except KeyError:
                new_string = new_string + letter
        else:
            new_string = new_string + letter
    return new_string      
    # ==============



print(vowel_swapper("aAa eEe iIi oOo uUu")) # Should print "a4a e3e i!i o000o u|_|u" to the console
print(vowel_swapper("Hello World")) # Should print "Hello Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "Ev3rything's Av4!lable" to the console
