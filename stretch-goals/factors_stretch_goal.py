def factors(number):
    # ==============
    # Your code here
    lst = []
    num = 1
    while num < (number - 1):
        num = num + 1
        if number%num == 0:
            lst.append(num)   
    if len(lst) == 0:
        return str(number) + ' is a prime number.'
    else:
        return lst
        
    # ==============

print(factors(15)) # Should print [3, 5] to the console
print(factors(12)) # Should print [2, 3, 4, 6] to the console
print(factors(13)) # Should print “13 is a prime number”
